#version 330 core
out vec4 FragColor;

in VS_OUT {
    vec3 FragPos;
    vec3 Normal;
    vec2 TexCoords;
    vec4 FragPosLightSpace;
} fs_in;

uniform sampler2D diffuseTexture;
uniform sampler2D shadowMap;

uniform vec3 lightPos;
uniform vec3 viewPos;


vec2 poissonDisk[16] = vec2[]( 
   vec2( -0.94201624, -0.39906216 ), 
   vec2( 0.94558609, -0.76890725 ), 
   vec2( -0.094184101, -0.92938870 ), 
   vec2( 0.34495938, 0.29387760 ), 
   vec2( -0.91588581, 0.45771432 ), 
   vec2( -0.81544232, -0.87912464 ), 
   vec2( -0.38277543, 0.27676845 ), 
   vec2( 0.97484398, 0.75648379 ), 
   vec2( 0.44323325, -0.97511554 ), 
   vec2( 0.53742981, -0.47373420 ), 
   vec2( -0.26496911, -0.41893023 ), 
   vec2( 0.79197514, 0.19090188 ), 
   vec2( -0.24188840, 0.99706507 ), 
   vec2( -0.81409955, 0.91437590 ), 
   vec2( 0.19984126, 0.78641367 ), 
   vec2( 0.14383161, -0.14100790 ) 
);


float calculateVisibility(vec4 fragPosLightSpace, vec3 lightDir, vec3 normal)
{
    float visibility = 1.0;

    vec3 projCoords = fragPosLightSpace.xyz / fragPosLightSpace.w;


    // transform to [0, 1] range
    projCoords = projCoords * 0.5 + 0.5;

    float cosTheta = clamp(dot(normal, lightDir), 0.0, 1.0);
    float bias = 0.0005*tan(acos(cosTheta));
    clamp(bias, 0, 0.01);

    if(projCoords.z > 1.0)
        return 1.0;


    for(int i=0; i<16; i++)
    {
        if(texture(shadowMap, projCoords.xy + poissonDisk[i]/1400.0).r < projCoords.z - bias)
        {
            visibility -= 0.05;
        }        
    }

    return visibility;
}
float ShadowCalculation(vec4 fragPosLightSpace, vec3 lightDir, vec3 normal)
{
    // perform perspective divide
    vec3 projCoords = fragPosLightSpace.xyz / fragPosLightSpace.w;

    // transform to [0, 1] range
    projCoords = projCoords * 0.5 + 0.5;
    // get closest depth value from light's perspective (using [0,1] range fragPosLight as coords)
    float closestDepth = texture(shadowMap, projCoords.xy).r;
    // get depth of current fragment from light's perspective
    float currentDepth = projCoords.z;

    float cosTheta = clamp(dot(normal, lightDir), 0.0, 1.0);
    // check whether current frag pos is in shadow
    //float bias = max(0.005 * (1.0 - dot(normal, lightDir)), 0.0005);  

    // check whether current frag pos is in shadow
    // float shadow = current depth - bias > closestDepth ? 1.0 : 0.0;
    // PCF
    float shadow = 0.0;
    vec2 texelSize = 1.0 / textureSize(shadowMap, 0);
    for(int x = -1; x <=1; ++x)
    {
        for(int y = -1; y <= 1; ++y)
        {
            float pcfDepth = texture(shadowMap, projCoords.xy + vec2(x,y) * texelSize).r;
            shadow += currentDepth > pcfDepth ? 1.0 : 0.0;
        }
    }
    shadow /= 9.0;

    if(projCoords.z > 1.0)
        return 0.0;

    return shadow;
}


void main()
{
    vec3 color = texture(diffuseTexture, fs_in.TexCoords).rgb;
    vec3 normal = normalize(fs_in.Normal);
    vec3 lightColor = vec3(0.3);
    // ambient
    vec3 ambient = 0.3 * lightColor;
    // diffuse
    vec3 lightDir = normalize(lightPos - fs_in.FragPos);
    float diff = max(dot(lightDir, normal), 0.0);
    vec3 diffuse = diff * lightColor;
    // specular
    vec3 viewDir = normalize(viewPos - fs_in.FragPos);
    float spec = 0.0;
    vec3 halfwayDir = normalize(lightDir + viewDir);
    spec = pow(max(dot(normal, halfwayDir), 0.0), 64.0);
    vec3 specular = spec * lightColor;
    // calculate shadow
    float visibility = calculateVisibility(fs_in.FragPosLightSpace, lightDir, normal);

    float shadow = ShadowCalculation(fs_in.FragPosLightSpace, lightDir, normal);
    vec3 lighting = (ambient + (visibility) * (diffuse + specular)) * color;

    FragColor = vec4(lighting, 1.0);

}