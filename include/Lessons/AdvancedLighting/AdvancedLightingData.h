#ifndef __ADVANCED_LIGHTING_DATA_H__
#define __ADVANCED_LIGHTING_DATA_H__

#include <vector>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <ShaderProgram.h>
#include <Model.h>

namespace AdvancedLightingData
{
    unsigned int SCR_WIDTH = 1440;
    unsigned int SCR_HEIGHT = 700;

    unsigned int planeVAO;

    unsigned int floorTexture;

    ShaderProgram *shader;

    glm::vec3 lightPos;

    bool blinn = false;
    bool blinnKeyPressed = false;

    float planeVertices[] =
    {
        // positions            // normals         // texcoords
         10.0f, -0.5f,  10.0f,  0.0f, 1.0f, 0.0f,  10.0f,  0.0f,
        -10.0f, -0.5f,  10.0f,  0.0f, 1.0f, 0.0f,   0.0f,  0.0f,
        -10.0f, -0.5f, -10.0f,  0.0f, 1.0f, 0.0f,   0.0f, 10.0f,

         10.0f, -0.5f,  10.0f,  0.0f, 1.0f, 0.0f,  10.0f,  0.0f,
        -10.0f, -0.5f, -10.0f,  0.0f, 1.0f, 0.0f,   0.0f, 10.0f,
         10.0f, -0.5f, -10.0f,  0.0f, 1.0f, 0.0f,  10.0f, 10.0f        
    };
}



#endif