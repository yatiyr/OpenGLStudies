#ifndef __LESSON_ESSENTIALS_H__
#define __LESSON_ESSENTIALS_H__

#include <math.h>
#include <iostream>
#include <algorithm>

// ---- OPENGL and GLFW ----- //
#include <glad/glad.h>
#include <GLFW/glfw3.h>
// ---------- GLM ----------- //
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>
// ---------- LOCAL --------- //
#include <Utils.h>
#include <Model.h>
#include <Camera.h>
#include <RootDir.h>
#include <ShaderProgram.h>
// ----------- STB ---------- //
#include <stb_image.h>
// -------------------------- //


#endif /* __LESSON_ESSENTIALS_H__ */