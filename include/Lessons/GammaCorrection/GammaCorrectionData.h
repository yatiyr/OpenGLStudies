#ifndef __GAMMA_CORRECTION_DATA_H__
#define __GAMMA_CORRECTION_DATA_H__

#include <vector>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <ShaderProgram.h>
#include <Model.h>

namespace GammaCorrectionData
{
    unsigned int SCR_WIDTH = 1440;
    unsigned int SCR_HEIGHT = 700;

    unsigned int planeVAO;
    unsigned int floorTexture;
    unsigned int floorTextureGammaCorrected;

    ShaderProgram *shader;

    glm::vec3 lightPos;

    bool gammaEnabled = false;
    bool gammaKeyPressed = false;

    float planeVertices[] =
    {
        // positions            // normals         // texcoords
         10.0f, -0.5f,  10.0f,  0.0f, 1.0f, 0.0f,  10.0f,  0.0f,
        -10.0f, -0.5f,  10.0f,  0.0f, 1.0f, 0.0f,   0.0f,  0.0f,
        -10.0f, -0.5f, -10.0f,  0.0f, 1.0f, 0.0f,   0.0f, 10.0f,

         10.0f, -0.5f,  10.0f,  0.0f, 1.0f, 0.0f,  10.0f,  0.0f,
        -10.0f, -0.5f, -10.0f,  0.0f, 1.0f, 0.0f,   0.0f, 10.0f,
         10.0f, -0.5f, -10.0f,  0.0f, 1.0f, 0.0f,  10.0f, 10.0f
    };

    glm::vec3 lightPositions[] = 
    {
        glm::vec3(-3.0f, 0.0f, 0.0f),
        glm::vec3(-1.0f, 0.0f, 0.0f),
        glm::vec3 (1.0f, 0.0f, 0.0f),
        glm::vec3 (3.0f, 0.0f, 0.0f)
    };

    glm::vec3 lightColors[] =
    {
        glm::vec3(0.25),
        glm::vec3(0.50),
        glm::vec3(0.75),
        glm::vec3(1.00)
    };

}


#endif