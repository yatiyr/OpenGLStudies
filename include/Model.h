#ifndef __MODEL_H__
#define __MODEL_H__

#include <vector>
#include <string>
#include <Mesh.h>
#include <ShaderProgram.h>
#include <Utils.h>

#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>
#include <stb_image.h> 

class Model
{
public:
    std::vector<Texture> textures_loaded;
    std::vector<Mesh> meshes;
    std::string directory;    
    std::string path;    
    bool gammaCorrection;

    Model(const char *path, bool gamma = false) : gammaCorrection(gamma)
    {
        this->path = path;
        loadModel(path);

    }

    void Draw(ShaderProgram &shader)
    {
        for(unsigned int i=0; i<meshes.size(); i++)
        {
            meshes[i].Draw(shader);
        }
    }

    void DrawNoTexture(ShaderProgram &shader, unsigned int cubemap)
    {
        for(unsigned int i=0; i<meshes.size(); i++)
        {
            meshes[i].DrawNoTexture(shader, cubemap);
        }
    }

private:
    

    void loadModel(std::string path)
    {
        Assimp::Importer import;
        const aiScene *scene = import.ReadFile(path, 
                                aiProcess_Triangulate |
                                aiProcess_GenSmoothNormals | 
                                aiProcess_FlipUVs |
                                aiProcess_CalcTangentSpace | 
                                aiProcess_JoinIdenticalVertices |
                                aiProcess_SortByPType);


        if(!scene || scene->mFlags & AI_SCENE_FLAGS_INCOMPLETE || !scene->mRootNode)
        {
            std::cout << "ERROR::ASSIMP::" << import.GetErrorString() << std::endl;
            return;
        }
        directory = path.substr(0, path.find_last_of('/'));
        processNode(scene->mRootNode, scene);
    }

    void processNode(aiNode *node, const aiScene* scene)
    {
        for(unsigned int i=0; i<node->mNumMeshes; i++)
        {
            aiMesh* mesh = scene->mMeshes[node->mMeshes[i]];
            meshes.push_back(processMesh(mesh, scene));
        }

        for(unsigned int i=0; i<node->mNumChildren; i++)
        {
            processNode(node->mChildren[i], scene);
        }
    }



    Mesh processMesh(aiMesh *mesh, const aiScene* scene)
    {
        std::vector<Vertex> vertices;
        std::vector<unsigned int> indices;
        std::vector<Texture> textures;

        for(unsigned int i=0; i<mesh->mNumVertices; i++)
        {
            Vertex vertex;

            // process vertex positions, normals and texture coordinates

            // process vertices
            glm::vec3 vector;
            vector.x = mesh->mVertices[i].x;
            vector.y = mesh->mVertices[i].y;
            vector.z = mesh->mVertices[i].z;
            vertex.Position = vector;

            
            // process normal
            if(mesh->mNormals != nullptr)
            {
                vector.x = mesh->mNormals[i].x;
                vector.y = mesh->mNormals[i].y;
                vector.z = mesh->mNormals[i].z;
                vertex.Normal = vector;
            }

            if(mesh->mTextureCoords[0])
            {
                glm::vec2 vec;
                vec.x = mesh->mTextureCoords[0][i].x;
                vec.y = mesh->mTextureCoords[0][i].y;
                vertex.TexCoords = vec;

                // tangent
                vector.x = mesh->mTangents[i].x;
                vector.y = mesh->mTangents[i].y;
                vector.z = mesh->mTangents[i].z;
                vertex.Tangent = vector;

                // bitangent
                vector.x = mesh->mBitangents[i].x;
                vector.y = mesh->mBitangents[i].y;
                vector.z = mesh->mBitangents[i].z;
                vertex.Bitangent = vector;
            }
            else
                vertex.TexCoords = glm::vec2(0.0f, 0.0f);


            vertices.push_back(vertex);
        }

        // process indices
        for(unsigned int i=0; i<mesh->mNumFaces; i++)
        {
            aiFace face = mesh->mFaces[i];
            for(unsigned int j=0; j<face.mNumIndices; j++)
            {
                indices.push_back(face.mIndices[j]);
            }

        }

        //process material
        if(mesh->mMaterialIndex >= 0)
        {
            aiMaterial* material = scene->mMaterials[mesh->mMaterialIndex];

            // get diffuse maps
            std::vector<Texture> diffuseMaps = loadMaterialTextures(material, aiTextureType_DIFFUSE, TextureType::texture_diffuse);
            textures.insert(textures.end(), diffuseMaps.begin(), diffuseMaps.end());

            // get specular maps
            std::vector<Texture> specularMaps = loadMaterialTextures(material, aiTextureType_SPECULAR, TextureType::texture_specular);
            textures.insert(textures.end(), specularMaps.begin(), specularMaps.end());

            // get normal maps
            std::vector<Texture> normalMaps = loadMaterialTextures(material, aiTextureType_HEIGHT, TextureType::texture_normal);
            textures.insert(textures.end(), normalMaps.begin(), normalMaps.end());

            // get height maps
            std::vector<Texture> heightMaps = loadMaterialTextures(material, aiTextureType_AMBIENT, TextureType::texture_height);
            textures.insert(textures.end(), heightMaps.begin(), heightMaps.end());
          
        }


        return Mesh(vertices, indices, textures);


    }

    std::vector<Texture> loadMaterialTextures(aiMaterial* mat, aiTextureType type, TextureType textype)
    {
        std::vector<Texture> textures;
        for(unsigned int i=0; i<mat->GetTextureCount(type); i++)
        {
            aiString str;
            mat->GetTexture(type, i, &str);
            bool skip = false;

            for(unsigned int j=0; j<textures_loaded.size(); j++)
            {
                if(std::strcmp(textures_loaded[j].path.data(), str.C_Str()) == 0)
                {
                    textures.push_back(textures_loaded[j]);
                    skip = true;
                    break;
                }
            }

            if(!skip)
            {
                Texture texture;
                texture.id = TextureFromFile(str.C_Str(), directory);
                texture.type = textype;
                texture.path = str.C_Str();
                textures.push_back(texture);
                textures_loaded.push_back(texture);
            }
        }
        return textures;
    }

};

#endif